//
//  EventTableViewCell.swift
//  Challenge
//
//  Created by William Rodriguez on 8/16/18.
//  Copyright © 2018 Vivid Seats. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {

    static let EVENT_TABLE_VIEW_CELL_NAME = "EventTableViewCell"
    
    @IBOutlet var uiLabelTop: UILabel!
    @IBOutlet var uiLabelMiddle: UILabel!
    @IBOutlet var imageViewEvent: UIImageView!
    @IBOutlet var uiLabelBottom: UILabel!
    @IBOutlet var uiLabelEventCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
