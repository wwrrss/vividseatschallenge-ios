//
//  EventsViewModel.swift
//  Challenge
//
//  Created by William Rodriguez on 8/16/18.
//  Copyright © 2018 Vivid Seats. All rights reserved.
//

import Foundation
import RxSwift
class EventsViewModel {
    
    var eventRepository:EventRepository? = nil
    
    let eventsSubject = PublishSubject<[Event]>()
    
    let disposable = DisposeBag()
    
    init(eventRepository:EventRepository) {
        self.eventRepository = eventRepository
        self.eventRepository?.getEventsSubject()
            .subscribe(
                onNext: { [weak self] (events) in
                            self?.eventsSubject.onNext(events)
                        }
            ).disposed(by: disposable)
    }
    
    func fetchEvents(){
        eventRepository?.fetchEvents()
    }
    
    
    
    
}
