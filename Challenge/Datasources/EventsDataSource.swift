//
//  EventsDataSource.swift
//  Challenge
//
//  Created by William Rodriguez on 8/16/18.
//  Copyright © 2018 Vivid Seats. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
class EventsDataSource:NSObject, UITableViewDataSource {
    
    var events:[Event] = [Event]()
    

    
    func addData(with:[Event]){
        events.removeAll()
        events.append(contentsOf: with)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let eventCell = tableView.dequeueReusableCell(withIdentifier: EventTableViewCell.EVENT_TABLE_VIEW_CELL_NAME) as? EventTableViewCell
            else {return UITableViewCell()}
        let event = events[indexPath.item]
        eventCell.uiLabelTop.text = event.topLabel
        eventCell.uiLabelMiddle.text = event.middleLabel
        eventCell.uiLabelBottom.text = event.bottomLabel
        eventCell.uiLabelEventCount.text = event.eventCount > 1 ?  "\(event.eventCount) events" : "1 event"
        eventCell.imageViewEvent.kf.setImage(with: URL(string: event.image))
        return eventCell
        
    }
    

    
    
    
}
