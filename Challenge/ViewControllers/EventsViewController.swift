//
//  EventsViewController.swift
//  Challenge
//
//  Created by William Rodriguez on 8/16/18.
//  Copyright © 2018 Vivid Seats. All rights reserved.
//

import UIKit
import RxSwift

class EventsViewController: UIViewController {
    
    let eventsRepository:EventRepository = EventRepositoryImp()
    
    let disposable = DisposeBag()
    
    let eventsTableDataSource = EventsDataSource()
    
    var eventsViewModel:EventsViewModel?
    
    @IBOutlet var eventsTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        eventsViewModel = EventsViewModel(eventRepository: eventsRepository)
        eventsViewModel?.fetchEvents()
        eventsViewModel?.eventsSubject.subscribe(
            onNext: {[weak self] (events) in
                self?.eventsTableDataSource.addData(with: events)
                self?.eventsTableView.reloadData()
            }
        ).disposed(by: disposable)

    }
    
    private func setupTableView(){
        let eventCell = UINib(nibName: EventTableViewCell.EVENT_TABLE_VIEW_CELL_NAME, bundle: nil)
        eventsTableView.register(eventCell, forCellReuseIdentifier: EventTableViewCell.EVENT_TABLE_VIEW_CELL_NAME)
        eventsTableView.dataSource = eventsTableDataSource
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
