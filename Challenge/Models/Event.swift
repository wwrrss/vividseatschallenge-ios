//
//  Event.swift
//  Challenge
//
//  Created by William Rodriguez on 8/16/18.
//  Copyright © 2018 Vivid Seats. All rights reserved.
//

import Foundation
import ObjectMapper

class Event: Mappable {
    var topLabel = ""
    var middleLabel = ""
    var bottomLabel = ""
    var eventCount = 0
    var image = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        topLabel <- map["topLabel"]
        middleLabel <- map["middleLabel"]
        bottomLabel <- map["bottomLabel"]
        eventCount <- map["eventCount"]
        image <- map["image"]
    }
}

