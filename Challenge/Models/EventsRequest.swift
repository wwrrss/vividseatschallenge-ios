//
//  EventsRequest.swift
//  Challenge
//
//  Created by William Rodriguez on 8/16/18.
//  Copyright © 2018 Vivid Seats. All rights reserved.
//

import Foundation
import ObjectMapper
class EventsRequest:Mappable {
    var startDate = Date()
    var endDate = Date()
    var includeSuggested = true
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        startDate <- (map["startDate"], CustomDateFormatter())
        endDate <- (map["endDate"], CustomDateFormatter())
        includeSuggested <- map["includeSuggested"]
    }
}
