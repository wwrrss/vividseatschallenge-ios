//
//  CustomDateFormatter.swift
//  Challenge
//
//  Created by William Rodriguez on 8/16/18.
//  Copyright © 2018 Vivid Seats. All rights reserved.
//

import Foundation
import ObjectMapper
class CustomDateFormatter: TransformType {
    
    let dateFormatter = DateFormatter()

    public init() {}
    
    func transformFromJSON(_ value: Any?) -> Date? {
        //i don't need this right now
        return Date()
    }
    
    func transformToJSON(_ value: Date?) -> String? {
        dateFormatter.dateFormat = "yyyy-MM-dd"
        guard let theDate = value else {return nil}
        return dateFormatter.string(from: theDate)
    }
}
