//
//  EventsRepository.swift
//  Challenge
//
//  Created by William Rodriguez on 8/16/18.
//  Copyright © 2018 Vivid Seats. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import RxSwift
class EventRepositoryImp:EventRepository{
    
    let eventsSubject = PublishSubject<[Event]>()
    
    func fetchEvents() {
        Alamofire.request(NetworkRouter.getEvents())
            .responseArray{ [weak self] (response: DataResponse<[Event]>) in
                response.result.ifSuccess {
                    guard let events = response.result.value else {return}
                    self?.eventsSubject.onNext(events)
                }
                
                response.result.ifFailure {
                    
                }
        }
        
    }
    
    func getEventsSubject() -> PublishSubject<[Event]> {
        return eventsSubject
    }
    
}

class MockEventRepository:EventRepository {
    
    let eventsSubject = PublishSubject<[Event]>()
    
    func fetchEvents() {
        let event1 = Event()
        event1.topLabel = "New England Patriots"
        let event2 = Event()
        event2.topLabel = "Atlanta Braves"
        var events = [Event]()
        events.append(event1)
        events.append(event2)
        eventsSubject.onNext(events)
    }
    
    func getEventsSubject() -> PublishSubject<[Event]> {
        return eventsSubject;
    }
    
    
}

protocol EventRepository {
    func fetchEvents()
    func getEventsSubject()->PublishSubject<[Event]>
}
