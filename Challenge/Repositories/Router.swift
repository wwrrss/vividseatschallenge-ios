//
//  Router.swift
//  Challenge
//
//  Created by William Rodriguez on 8/16/18.
//  Copyright © 2018 Vivid Seats. All rights reserved.
//

import Foundation
import Alamofire
enum NetworkRouter: URLRequestConvertible {
    
    static let baseUrl = "https://webservices.vividseats.com/rest/"
    
    case getEvents()
    
    var method:HTTPMethod {
        switch self {
            case .getEvents():
                return .post
        }
    }
    
    var path:String {
        switch self {
            case .getEvents():
                return "mobile/v1/home/cards"
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let urlString = NetworkRouter.baseUrl + path
        let url = try urlString.asURL()
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("iOS", forHTTPHeaderField: "X-Mobile-Platform")
        switch self {
        case .getEvents():
            let reqBody = EventsRequest()
            reqBody.endDate = Calendar.current.date(byAdding: .day, value: 10, to: reqBody.startDate)!
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: reqBody.toJSON(), options: [])
            return urlRequest
        }
    }
}
