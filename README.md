## Vivid Seats Challenge

This app consumes an api rest endpoint provided by VividSeats and shows the result in a TableView. 

App Architecture: MVVM

* Libraries used:
	* Alamofire (Networking)
	* RxSwift (Reactive programming)
	* Kingfisher (ImageDownload)
	* ObjectMapper (Json Serialization/Deserialization)


