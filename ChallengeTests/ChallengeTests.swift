//
//  ChallengeTests.swift
//  ChallengeTests
//
//  Created by William Rodriguez on 8/15/18.
//  Copyright © 2018 Vivid Seats. All rights reserved.
//

import XCTest

@testable import Challenge
import RxSwift


class ChallengeTests: XCTestCase {
    
    var eventsViewModel:EventsViewModel?
    
    override func setUp() {
        super.setUp()
        let eventsRepository:EventRepository = MockEventRepository()
        eventsViewModel = EventsViewModel(eventRepository: eventsRepository)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        let observableToTest = eventsViewModel?.eventsSubject
        
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
